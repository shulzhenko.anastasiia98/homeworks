import React from "react";
import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import CartPage from "./pages/CartPage/CartPage";
import FavouritePage from "./pages/FavouritePage/FavouritePage";

const AppRoutes = ({
  firstModalDisplay,
  addToFavourites,
  favourites,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  deleteCard,
  modalAct,
  setModalAct,
}) => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage
            firstModalDisplay={firstModalDisplay}
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
            modalAct={modalAct}
            setModalAct={setModalAct}
          />
        }
      />
      <Route
        path="/cart"
        element={
          <CartPage
            firstModalDisplay={firstModalDisplay}
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
            deleteCard={deleteCard}
            modalAct={modalAct}
            setModalAct={setModalAct}
          />
        }
      />
      <Route
        path="/favourite"
        element={
          <FavouritePage
            firstModalDisplay={firstModalDisplay}
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            addToFavourites={addToFavourites}
            modalAct={modalAct}
            setModalAct={setModalAct}
          />
        }
      />
    </Routes>
  );
};

export default AppRoutes;
