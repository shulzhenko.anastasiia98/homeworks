import React, { useEffect } from "react";
import CartContainer from "../../components/CartContainer/CartContainer";
import styles from "./CartPage.module.scss";

const CartPage = ({
  firstModalDisplay,
  favourites,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  addToFavourites,
  deleteCard,
  setModalAct,
  modalAct,
}) => {

  useEffect(() => {
    setModalAct("Delete from cart");
  })

  if (
    !JSON.parse(localStorage.getItem("carts")) ||
    JSON.parse(localStorage.getItem("carts")).length === 0
  ) {
    return <h1 className={styles.title}>Cart is empty!</h1>;
  }

  const cartsArticles = [];

  carts.forEach(({ article }) => {
    cartsArticles.push(article);
  });

  const cardsIntoCart = cards.filter((card) => {
    return cartsArticles.includes(card.article.toString());
  });

  return (
    <main>
      <section>
        <CartContainer
          firstModalDisplay={firstModalDisplay}
          favourites={favourites}
          addToCart={addToCart}
          cards={cards}
          carts={carts}
          openFirstModal={openFirstModal}
          setModalProps={setModalProps}
          addToFavourites={addToFavourites}
          cardsIntoCart={cardsIntoCart}
          deleteCard={deleteCard}
          modalAct={modalAct}
        />
      </section>
    </main>
  );
};

export default CartPage;
