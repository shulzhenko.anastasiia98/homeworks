import React, { useEffect } from "react";
import CardContainer from "../../components/CardContainer/CardContainer";

const HomePage = ({
  firstModalDisplay,
  favourites,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  addToFavourites,
  toAdd,
  modalAct,
  setModalAct,
}) => {

  useEffect(() => {
    setModalAct("Add to cart");
  })

  return (
    <main>
      <section>
        <CardContainer
          firstModalDisplay={firstModalDisplay}
          favourites={favourites}
          addToCart={addToCart}
          cards={cards}
          carts={carts}
          openFirstModal={openFirstModal}
          setModalProps={setModalProps}
          addToFavourites={addToFavourites}
          toAdd={toAdd}
          modalAct={modalAct}
        />
      </section>
    </main>
  );
};

export default HomePage;
