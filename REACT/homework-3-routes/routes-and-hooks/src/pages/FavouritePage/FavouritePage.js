import React, { useEffect } from "react";
import FavouriteContainer from "../../components/FavouriteContainer/FavouriteContainer";
import styles from "./FavouritePage.module.scss";

const FavouritePage = ({
  firstModalDisplay,
  favourites,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  addToFavourites,
  modalAct,
  setModalAct,
}) => {
  useEffect(() => {
    setModalAct("Add to cart");
  })

  if (
    !JSON.parse(localStorage.getItem("favourites")) ||
    JSON.parse(localStorage.getItem("favourites")).length === 0
  ) {
    return <h1 className={styles.title}>There are no favourites</h1>;
  }
  
  const favouriteCards = cards.filter((card) => {
    return favourites.includes(card.article.toString());
  });

  return (
    <main>
      <section>
        <FavouriteContainer
          firstModalDisplay={firstModalDisplay}
          favourites={favourites}
          addToCart={addToCart}
          cards={cards}
          carts={carts}
          openFirstModal={openFirstModal}
          setModalProps={setModalProps}
          addToFavourites={addToFavourites}
          favouriteCards={favouriteCards}
          modalAct={modalAct}
          setModalAct={setModalAct}
        />
      </section>
    </main>
  );
};

export default FavouritePage;
