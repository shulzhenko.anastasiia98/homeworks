import { useState, useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import AppRoutes from "./AppRoutes";
import Button from "./components/Button/Button";
import Header from "./components/Header/Header";
import Modal from "./components/Modal/Modal";

const App = () => {
  const [firstModalDisplay, setFirstModalDisplay] = useState(false);
  const [favourites, setFavourites] = useState([]);
  const [cards, setCards] = useState([]);
  const [carts, setCarts] = useState([]);
  const [modalProps, setModalProps] = useState({});
  const [modalAct, setModalAct] = useState("Add to cart");

  useEffect(() => {
    const getData = async () => {
      const cards = await fetch(`./items.json`).then((res) => res.json());
      cards.forEach((card) => {
        card.isFavourite =
          localStorage.getItem("Favourites") &&
          JSON.parse(localStorage.getItem("Favourites")).includes(card.article);
        setCards(cards);
        if (localStorage.getItem("carts")) {
          setCarts(JSON.parse(localStorage.getItem("carts")));
        }
        if (localStorage.getItem("favourites")) {
          setFavourites(JSON.parse(localStorage.getItem("favourites")));
        }
      });
    };
    getData();
  }, []);

  const openFirstModal = (message) => {
    if (message === "Add to cart") {
      setModalAct("Add to cart");
    }
    if (message === "Delete from cart") {
      setModalAct("Delete from cart");
    }
    setFirstModalDisplay(true);
  };

  const closeFirstModal = () => {
    setFirstModalDisplay(false);
  };

  const addToCart = (article) => {
    setCarts((current) => {
      const carts = [...current];
      const index = carts.findIndex((el) => el === article);

      if (index === -1) {
        carts.push({ article: article, count: 1 });
      } else {
        carts[index].count += 1;
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      closeFirstModal();
      return carts;
    });
  };

  const addToFavourites = (article) => {
    setFavourites((current) => {
      let favourites = [...current];
      if (current.includes(article)) {
        favourites = current.filter((el) => {
          return el !== article;
        });
      } else {
        favourites = [...current, article];
      }
      localStorage.setItem("favourites", JSON.stringify(favourites));
      closeFirstModal();
      return favourites;
    });
  };

  const deleteCard = (article) => {
    setCarts((current) => {
      const carts = [...current];

      const index = carts.findIndex((el) => el.article === article);

      if (index !== -1) {
        carts.splice(index, 1);
      }

      localStorage.setItem("carts", JSON.stringify(carts));
      closeFirstModal();
      return carts;
    });
  };

  return (
    <BrowserRouter>
      <>
        <Header carts={carts} favourites={favourites} />
        <section>
          <AppRoutes
            firstModalDisplay={firstModalDisplay}
            favourites={favourites}
            addToCart={addToCart}
            cards={cards}
            carts={carts}
            openFirstModal={openFirstModal}
            setModalProps={setModalProps}
            modalAct={modalAct}
            setModalAct={setModalAct}
            addToFavourites={addToFavourites}
            deleteCard={deleteCard}
          />
        </section>

        {firstModalDisplay && (
          <Modal
            className="modal"
            firstModalDisplay={firstModalDisplay}
            closeButton={true}
            close={closeFirstModal}
            modalProps={modalProps}
            modalAct={modalAct}
            addToCart={addToCart}
            actions={[
              <Button
                key={1}
                onClick={() => {
                  if (modalAct === "Add to cart") {
                    addToCart(modalProps.article);
                  } else if (modalAct === "Delete from cart") {
                    deleteCard(modalProps.article);
                  }
                }}
                className="modal-button"
                text="OK"
              />,
              <Button
                key={2}
                onClick={closeFirstModal}
                className="modal-button"
                text="CANCEL"
              />,
            ]}
          />
        )}
      </>
    </BrowserRouter>
  );
};
export default App;
