import styles from "./Modal.module.scss";
import PropTypes from "prop-types";

const Modal = ({
  firstModalDisplay,
  closeButton,
  actions,
  close,
  modalProps,
  modalAct,
}) => {
  if (firstModalDisplay) {
    return (
      <div className={styles.modalBackground} onClick={close}>
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
          className={styles.modalContent}
        >
          <header className={styles.modalContentHeader}>
            {closeButton && (
              <div onClick={close} className={styles.modalCloseBtn}>
                &times;
              </div>
            )}
          </header>
          <div>
            <p className={styles.modalContentText}>
              {`${modalAct} ${modalProps.name} ?`}
            </p>
            <div className={styles.modalButtons}>{actions}</div>
          </div>
        </div>
      </div>
    );
  }
};

export default Modal;

Modal.propTypes = {
  firstModalDisplay: PropTypes.bool,
  closeButton: PropTypes.bool,
  close: PropTypes.func,
  modalAct: PropTypes.string,
};
