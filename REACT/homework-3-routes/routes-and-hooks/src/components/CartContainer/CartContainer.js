import React from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./CartContainer.module.scss";

const CartContainer = ({
  firstModalDisplay,
  addToCart,
  cards,
  carts,
  openFirstModal,
  setModalProps,
  addToFavourites,
  favourites,
  cardsIntoCart,
  deleteCard,
  modalAct,
}) => {
  return (
    <>
      <div className={styles.titleContainer}>
        <h1>CART</h1>
      </div>
      <div>
        <ul className={styles.list}>
          {cardsIntoCart.map(({ name, path, price, article, isFavourite }) => (
            <li key={article}>
              <CardItem
                favourite={favourites.includes(article)}
                addToCart={addToCart}
                article={article}
                name={name}
                price={price}
                path={path}
                isFavourite={isFavourite}
                openFirstModal={openFirstModal}
                setModalProps={setModalProps}
                addToFavourites={addToFavourites}
                firstModalDisplay={firstModalDisplay}
                deleteCard={deleteCard}
                modalAct={modalAct}
              />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default CartContainer;
