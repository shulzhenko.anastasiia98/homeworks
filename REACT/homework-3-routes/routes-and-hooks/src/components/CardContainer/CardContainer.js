import CardItem from "../CardItem/CardItem";
import styles from "./CardContainer.module.scss";

const CardContainer = ({
  firstModalDisplay,
  addToCart,
  cards,
  openFirstModal,
  setModalProps,
  addToFavourites,
  favourites,
  modalAct,
}) => {
  return (
    <>
      <div className={styles.titleContainer}>
        <h1>AVAILABLE CATALOG</h1>
      </div>
      <div>
        <ul className={styles.list}>
          {cards.map(({ name, path, price, article, isFavourite }) => (
            <li key={article}>
              <CardItem
                favourite={favourites.includes(article)}
                addToCart={addToCart}
                article={article}
                name={name}
                price={price}
                path={path}
                isFavourite={isFavourite}
                openFirstModal={openFirstModal}
                setModalProps={setModalProps}
                addToFavourites={addToFavourites}
                firstModalDisplay={firstModalDisplay}
                modalAct={modalAct}
              />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default CardContainer;
