import styles from "./Header.module.scss";
import cartIcon from "../../components/icons/cart-outline.svg";
import favIcon from "../../components/icons/services_favorite_favorite_star_9891.png";
import Navigation from "../Navigation/Navigation";
import { NavLink } from "react-router-dom";

const Header = () => {
  let cartQt;
  let favouriteQt;
  if (localStorage.getItem("carts")) {
    cartQt = <span>{JSON.parse(localStorage.getItem("carts")).length}</span>;
  } else {
    cartQt = <span>0</span>;
  }

  if (localStorage.getItem("favourites")) {
    favouriteQt = (
      <span>{JSON.parse(localStorage.getItem("favourites")).length}</span>
    );
  } else {
    favouriteQt = <span>0</span>;
  }

  return (
    <header className={styles.root}>
      <Navigation />
      <div>
        <ul>
          <li>
            <NavLink to="/cart">
              <img src={cartIcon} alt="Cart" />
            </NavLink>
          </li>
          <li>{cartQt}</li>
        </ul>
        <ul>
          <li>
            <NavLink to="/favourite">
              <img src={favIcon} alt="Cart" width={26} />
            </NavLink>
          </li>
          <li>{favouriteQt}</li>
        </ul>
      </div>
    </header>
  );
};

export default Header;
