import React from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./FavouriteContainer.module.scss";

const FavouriteContainer = ({
  firstModalDisplay,
  addToCart,
  cards,
  openFirstModal,
  setModalProps,
  addToFavourites,
  favourites,
  favouriteCards,
  modalAct,
  setModalAct,
}) => {
  return (
    <>
      <div className={styles.titleContainer}>
        <h1>FAVOURITES</h1>
      </div>
      <div>
        <ul className={styles.list}>
          {favouriteCards.map(({ name, path, price, article, isFavourite }) => (
            <li key={article}>
              <CardItem
                favourite={favourites.includes(article)}
                addToCart={addToCart}
                article={article}
                name={name}
                price={price}
                path={path}
                isFavourite={isFavourite}
                openFirstModal={openFirstModal}
                setModalProps={setModalProps}
                addToFavourites={addToFavourites}
                firstModalDisplay={firstModalDisplay}
                modalAct={modalAct}
                setModalAct={setModalAct}
              />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default FavouriteContainer;
