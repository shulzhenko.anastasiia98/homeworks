import React from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./FavouriteContainer.module.scss";


const FavouriteContainer = ({ favourites }) => {

  return (
    <>
      <div className={styles.titleContainer}>
        <h1>FAVOURITES</h1>
      </div>
      <div>
        <ul className={styles.list}>
          {favourites.map((item) => (
            <li key={item.path}>
              <CardItem item={item} toCart />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default FavouriteContainer;
