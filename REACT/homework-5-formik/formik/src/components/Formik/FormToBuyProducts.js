import React from "react";
import { Formik, Form, } from "formik";
import Button from "../Button/Button";
import styles from "./FormToBuyProducts.module.scss";
import * as yup from "yup";
import CustomInput from "../CustomInput/CustomInput";
import { useDispatch } from "react-redux";
import { REMOVE_FROM_TRASH } from "../../store/types";
import { getTrash, setTrash } from "../../store/operations";

const FormToBuyProducts = () => {
  const initialValues = {
    firstName: "",
    lastName: "",
    age: "",
    address: "",
    phoneNumber: "",
  };

  const dispatch = useDispatch();

  const cleanTrash = (data) => {
    dispatch({ type: REMOVE_FROM_TRASH, payload: data })
  }

  const handleSubmit = (values, { resetForm }) => {
    const data = getTrash();
    console.log(data);

    Object.keys(data).forEach((item) => {
        cleanTrash(item)
    })
    setTrash({})
    console.log(values);
    resetForm();
  }


  const validationSchema = yup.object().shape({
    firstName: yup
      .string()
      .min(3, "Min 3 symbols")
      .max(20, "Max 20 symbols")
      .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ")
      .required("First name field is required"),


      lastName: yup
      .string()
      .min(3, "Min 3 symbols")
      .max(20, "Max 20 symbols")
      .matches(/^[aA-zZ\s]+$/, "Only alphabets are allowed for this field ")
      .required("Last name field is required"),

      age: yup
      .number()
      .min(10, "Your age should be greater than 10")
      .max(110, "Your age should be less than 110")
      .required("Age field is required"),

      address: yup
      .string()
      .min(3, "Min 3 symbols")
      .max(20, "Max 20 symbols")
      .required("First name field is required"),

      phoneNumber: yup
      .number()
      .required("First name field is required"),
      
  });

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      onSubmit={handleSubmit}
      validateOnBlur={true}
    >
      {({ isValid }) => {
        return (
          <Form className={styles.formToBuyProducts}>
            <h2>BUY PRODUCTS</h2>

            <CustomInput type="text" name="firstName" placeholder="First name" />

            <CustomInput type="text" name="lastName" placeholder="Last name" />

            <CustomInput type="text" name="age" placeholder="Age" />

            <CustomInput type="text" name="address" placeholder="Address" />

            <CustomInput type="text" name="phoneNumber" placeholder="Phone number" />

            <Button text="Checkout" type="submit" disabled={ isValid ? true : false }/>
          </Form>
        );
      }}
    </Formik>
  );
};

export default FormToBuyProducts;
