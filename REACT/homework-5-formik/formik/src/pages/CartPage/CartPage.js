import React from "react";
import CartContainer from "../../components/CartContainer/CartContainer";
import styles from "./CartPage.module.scss";
import { useSelector, shallowEqual } from "react-redux";
import FormToBuyProducts from "../../components/Formik/FormToBuyProducts";

const CartPage = () => {
  const items = useSelector((state) => state.items.data, shallowEqual);
  const trash = items.filter((item) => item.inTrashAmount);


  if(
    !JSON.parse(localStorage.getItem("Trash")) ||
    JSON.parse(localStorage.getItem("Trash")).length === 0 ||
    !Object.keys(JSON.parse(localStorage.getItem("Trash"))) ||
    Object.keys(JSON.parse(localStorage.getItem("Trash"))).length === 0 

  ){
    return <h1 className={styles.title}>Cart is empty!</h1>;
  }

  return (
    <main>
      <div className={styles.titleContainer}>
        <h1>CART</h1>
      </div>
      <section className={styles.contentContainer}>
        <CartContainer trash={trash} />
        <FormToBuyProducts />
      </section>
    </main>
  );
};

export default CartPage;
