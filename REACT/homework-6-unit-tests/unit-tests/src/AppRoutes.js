import React from "react";
import { Routes, Route } from "react-router-dom";
import HomePage from "./pages/HomePage/HomePage";
import CartPage from "./pages/CartPage/CartPage";
import FavouritePage from "./pages/FavouritePage/FavouritePage";

const AppRoutes = () => {
  return (
    <Routes>
      <Route
        path="/"
        element={
          <HomePage />
        }
      />
      <Route
        path="/cart"
        element={
          <CartPage />
        }
      />
      <Route
        path="/favourite"
        element={
          <FavouritePage />
        }
      />
    </Routes>
  );
};

export default AppRoutes;
