import React from "react";
import styles from "./ProductsTable.module.scss"

const ProductsTable = ({ products }) => {
   console.log(products);
   return (
      <div className={styles.tableContainer}>
         <table>
            <thead>
               <tr>
                  <th>Name</th>
                  <th>Image</th>
                  <th>Price</th>
                  <th>Color</th>
               </tr>
            </thead>
            <tbody>
               {products.map(({ name, path, color, price, article }) => {
                  return (
                     <tr key={article}>
                        <td>{name}</td>
                        <td><img width="30px" src={path} /></td>
                        <td>{price}</td>
                        <td>{color}</td>
                     </tr>
                  )
               })}
            </tbody>
         </table>
      </div>
   )

}

export default ProductsTable;