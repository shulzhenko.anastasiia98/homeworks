import React, { memo } from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./CartContainer.module.scss";

const CartContainer = ({ trash }) => {
  return (
    <>

      <div>
        <ul className={styles.list}>
          {trash.map((item) => (
            <li key={item.article}>
              <CardItem item={item} fromCart />
            </li>
          ))}
        </ul>
      </div>
    </>
  );
};

export default memo(CartContainer);
