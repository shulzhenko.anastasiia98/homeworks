import React, { memo } from "react";
import styles from "./Modal.module.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

const Modal = ({
  closeModal,
  closeButton,
  actions,
}) => {

  const { info: action, data: item } = useSelector((state) => state.modal);

  return (
    <div className={styles.modalBackground} onClick={closeModal}>
      <div
        onClick={(e) => {
          e.stopPropagation();
        }}
        className={styles.modalContent}
      >
        <header className={styles.modalContentHeader}>
          {closeButton && (
            <div onClick={closeModal} className={styles.modalCloseBtn}>
              &times;
            </div>
          )}
        </header>
        <div>
          <p className={styles.modalContentText}>
            {`${action} ${item.name} ?`}
          </p>
          <div className={styles.modalButtons}>{actions}</div>
        </div>
      </div>
    </div>
  );
};

export default memo(Modal);

Modal.propTypes = {
  closeButton: PropTypes.bool,
  closeModal: PropTypes.func,
  actions: PropTypes.array,
};
