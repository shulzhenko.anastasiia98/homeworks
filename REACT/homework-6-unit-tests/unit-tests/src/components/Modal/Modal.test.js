
import { render, fireEvent, screen } from "@testing-library/react";
import Modal from "./Modal";
import userEvent from "@testing-library/user-event";

jest.mock("react-redux", () => ({
   useSelector: () => {
      return { info: "testAction", data: { name: "itemName" } };
   },
}));

const idModal = "modalJest";
const idButton = "closeButtonJest";

describe("Modal.js", () => {
   test("Should Modal Render", () => {
      render(<Modal />);
   });

   test("Try modal Action", () => {
      const action = ["Action"];
      render(<Modal actions={action} />);
   });
});