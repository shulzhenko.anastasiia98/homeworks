import Header from './Header';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '../../store/store';

const HeaderComponent = () => (
   <BrowserRouter>
      <Provider store={store}>
         <Header />
      </Provider>
   </BrowserRouter>
);

describe('Header snapshot testing', () => {
   test('should Header match snapshot', () => {
      const { asFragment } = render(<HeaderComponent />);

      expect(asFragment()).toMatchSnapshot();
   });
});
