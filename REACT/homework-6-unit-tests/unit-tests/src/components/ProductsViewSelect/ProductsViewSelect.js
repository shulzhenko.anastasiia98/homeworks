import React, { useState, useContext } from 'react';
import ProductsContext from '../../contexts/ProductsContext/ProductsContext';
import styles from "./ProductsViewSelect.module.scss"

const ProductsViewSelect = () => {
   const { currentProductView, setCurrentProductView } = useContext(ProductsContext);

   return (
      <div className={styles.selectContainer}>
         <select
            name="productsView"
            id="productsView"
            value={currentProductView}
            onChange={({ target: { value } }) => setCurrentProductView(value)}
         >
            <option value='cards'>Cards</option>
            <option value='table'>Table</option>
         </select>
      </div>
   );
};

export default ProductsViewSelect;
