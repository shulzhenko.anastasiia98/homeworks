import React, { memo, useContext } from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./CardContainer.module.scss";
import { useSelector, shallowEqual } from "react-redux";
import ProductsViewSelect from "../ProductsViewSelect/ProductsViewSelect";
import ProductsContext from "../../contexts/ProductsContext/ProductsContext";
import ProductsTable from "../ProductsTable/ProductsTable";

const CardContainer = () => {
  const cards = useSelector((state) => state.items.data, shallowEqual);
  console.log(cards);
  const { currentProductView } = useContext(ProductsContext);
  return (
    <>
      <div className={styles.titleContainer}>
        <h1>AVAILABLE CATALOG</h1>
        <ProductsViewSelect />
      </div>

      {currentProductView === "cards" && <div>
        <ul className={styles.list}>
          {cards.map((item) => (
            <li key={item.path}>
              <CardItem key={item.article} item={item} toCart />
            </li>
          ))}
        </ul>
      </div>}

      {currentProductView === "table" && <ProductsTable products={cards} />}
    </>
  );
};

export default memo(CardContainer);
