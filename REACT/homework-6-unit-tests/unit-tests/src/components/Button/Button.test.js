import Button from "./Button";
import { render, screen, fireEvent } from '@testing-library/react';

describe("Button renders", () => {
   test("Should Button render without props", () => {
      const { asFragment } = render(<Button />);
      expect(asFragment()).toMatchSnapshot()
   })

   test("Should Button render with props", () => {
      const { asFragment } = render(<Button text="test-text" onClick={() => { }} />);
      expect(asFragment()).toMatchSnapshot()
   })
})

const handleClick = jest.fn();

describe('Button handle click', () => {
   test('Should onClick works', () => {
      render(<Button onClick={handleClick} text={'test'} />);

      const button = screen.getByText('test');
      fireEvent.click(button);

      expect(handleClick).toHaveBeenCalled();
   });
});