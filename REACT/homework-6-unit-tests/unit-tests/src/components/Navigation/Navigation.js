import React, { memo } from "react";
import { NavLink } from "react-router-dom";
import styles from "./Navigation.module.scss";

const Navigation = () => {
  return (
    <nav className={styles.navigation}>
      <ul>
        <li>
          <NavLink className={styles.navItem} to="/">
            HOME
          </NavLink>
        </li>
        <li>
          <NavLink className={styles.navItem} to="/cart">
            CART
          </NavLink>
        </li>
        <li>
          <NavLink className={styles.navItem} to="/favourite">
            FAVOURITE
          </NavLink>
        </li>
      </ul>
    </nav>
  );
};

export default memo(Navigation);
