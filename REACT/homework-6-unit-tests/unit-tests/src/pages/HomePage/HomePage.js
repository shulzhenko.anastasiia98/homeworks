import React, { memo } from "react";
import CardContainer from "../../components/CardContainer/CardContainer";


const HomePage = () => {
  return (
    <main>
      <section>
        <CardContainer />
      </section>
    </main>
  );
};

export default memo(HomePage);
