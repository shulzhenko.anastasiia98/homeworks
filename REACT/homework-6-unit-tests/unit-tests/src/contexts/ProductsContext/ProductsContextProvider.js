import { useState } from "react";
import ProductsContext from "./ProductsContext";


const ProductsContextProvider = ({ children }) => {

   const [currentProductView, setCurrentProductView] = useState("cards");

   return (
      <ProductsContext.Provider value={{ currentProductView, setCurrentProductView }}>
         {children}

      </ProductsContext.Provider>
   )
}

export default ProductsContextProvider;