import React from "react";
import FavouriteContainer from "../../components/FavouriteContainer/FavouriteContainer";
import styles from "./FavouritePage.module.scss";
import { useSelector, shallowEqual } from "react-redux";




const FavouritePage = () => {
  const items = useSelector((state) => state.items.data, shallowEqual);
  const favourites = items.filter((item) => item.favourite);

  if (
    !JSON.parse(localStorage.getItem("Favourite")) ||
    JSON.parse(localStorage.getItem("Favourite")).length === 0
  ) {
    return <h1 className={styles.title}>There are no favourites</h1>;
  }
  
  return (
    <main>
      <section>
        <FavouriteContainer favourites={favourites}/>
      </section>
    </main>
  );
};

export default FavouritePage;
