import React from "react";
import CartContainer from "../../components/CartContainer/CartContainer";
import styles from "./CartPage.module.scss";
import { useSelector, shallowEqual } from "react-redux";

const CartPage = () => {
  const items = useSelector((state) => state.items.data, shallowEqual);
  const trash = items.filter((item) => item.inTrashAmount);


  if(
    !Object.keys(JSON.parse(localStorage.getItem("Trash"))) ||
    Object.keys(JSON.parse(localStorage.getItem("Trash"))).length === 0
  ){
    return <h1 className={styles.title}>Cart is empty!</h1>;
  }

  return (
    <main>
      <section>
        <CartContainer trash={trash} />
      </section>
    </main>
  );
};

export default CartPage;
