import styles from "./CardItem.module.scss";
import Button from "../Button/Button";
import PropTypes from "prop-types";
import starIcon from "../../components/icons/star_favorite_favorite_favorite_favorite_2359.png";
import outlineStarIcon from "../../components/icons/services_favorite_favorite_star_9891.png";
import { useDispatch } from "react-redux";
import { SHOW_MODAL, ADD_REMOVE_FAVORITE } from "../../store/types";

const CardItem = ({
  item,
  toCart,
  fromCart
}) => {


  const { name, path, article, price, favourite } = item;
  const dispatch = useDispatch();

  const addToFavourite = (id) => {
    dispatch({ type: ADD_REMOVE_FAVORITE, payload: id });
  };

  const showModal = (info, data) => {
    dispatch({ type: SHOW_MODAL, payload: { info, data } });
  };



  return (
    <div className={styles.card}>
      <button type="button" className={styles.likeButton}>
        <img
          onClick={() => {
            addToFavourite(article);
          }}
          src={favourite ? outlineStarIcon : starIcon}
          alt="Favourite"
        />
      </button>
      <span className={styles.title}>{name}</span>
      <img className={styles.itemAvatar} src={path} alt={name} />
      <span className={styles.description}>{price}</span>

      <div className={styles.btnContainer}>
        <div className="app__buttons">
          {toCart && (
            <Button
              backgroundColor="green"
              text="Add to cart"
              className="modalButton"
              onClick={() => {
                showModal("Add to trash", item)
              }}
            />
          )}
          {fromCart && (
            <Button
              text="Delete from cart"
              backgroundColor="red"
              className="modalButton"
              onClick={() => {
                showModal("Remove from trash", item)
              }}
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default CardItem;

CardItem.propTypes = {
  item: PropTypes.shape({
    name: PropTypes.string,
    path: PropTypes.string,
    price: PropTypes.string,
    article: PropTypes.string,
    color: PropTypes.string,
    favourite: PropTypes.bool,
  }),

  toCart: PropTypes.bool,
  fromCart: PropTypes.bool,
};
