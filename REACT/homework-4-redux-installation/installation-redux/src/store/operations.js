import { SUCCESS } from "./types";

export const getTrash = () => {
  const trash = localStorage.getItem("Trash");
  return trash ? JSON.parse(trash) : {};
};

export const setTrash = (data) => {
  localStorage.setItem("Trash", JSON.stringify(data));
  console.log(JSON.parse(localStorage.getItem("Trash")));
};

export const getFavorites = () => {
  const favouritesLS = localStorage.getItem("Favourite");
  return favouritesLS ? JSON.parse(favouritesLS) : [];
};

export const setFavorites = (data) => {
  localStorage.setItem("Favourite", JSON.stringify(data));
  // console.log(JSON.parse(localStorage.getItem("Favourite")));

};

export const loadItems = () => async (dispatch) => {
  const favourites = getFavorites();
  const trash = getTrash();

  try {
    const data = await fetch(`./items.json`).then((res) => res.json());

    const newItems = data.map((item) => {
      item.favourite = favourites.includes(item.article);
      item.inTrashAmount = trash[item.article] || null;
      return item;
    });
    dispatch({ type: SUCCESS, payload: newItems });
  } catch (error) {
    console.log(error);
  }
};
