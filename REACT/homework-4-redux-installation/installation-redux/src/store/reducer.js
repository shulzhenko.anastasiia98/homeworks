import {
  SUCCESS,
  SHOW_MODAL,
  ADD_REMOVE_FAVORITE,
  CLOSE_MODAL,
  ADD_TO_TRASH,
  REMOVE_FROM_TRASH,
} from "./types";
import { getTrash, setTrash, getFavorites, setFavorites } from "./operations";
import produce from "immer";

const initialState = {
  items: {
    data: [],
    isLoading: true,
  },
  modal: {
    info: "",
    data: null,
    isOpen: false,
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SUCCESS:
      return {
        ...state,
        items: { ...state.data, data: action.payload, isLoading: false },
      };
    case SHOW_MODAL:
      return {
        ...state,
        modal: {
          ...state.modal,
          info: action.payload.info,
          data: action.payload.data,
          isOpen: true,
        },
      };
    case CLOSE_MODAL:
      return { ...state, modal: { ...state.modal, isOpen: false } };
    case ADD_REMOVE_FAVORITE:
      return produce(state, (draftState) => {
        draftState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.favourite = !el.favourite;

            const favourites = getFavorites();
            if (favourites.includes(el.article)) {
              favourites.splice(favourites.indexOf(el.article), 1);
            } else {
              favourites.push(el.article);
            }
            setFavorites(favourites);
          }
          return el;
        });
      });

    case ADD_TO_TRASH:
      return produce(state, (draftState) => {
        draftState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.inTrashAmount++;
            const trash = getTrash();
            if (Object.keys(trash).includes(el.article)) {
              trash[el.article]++;
            } else {
              trash[el.article] = 1;
            }
            setTrash(trash);
          }
          return el;
        });
      });

    case REMOVE_FROM_TRASH:
      return produce(state, (draftState) => {
        draftState.items.data.forEach((el) => {
          if (el.article === action.payload) {
            el.inTrashAmount--;
            const trash = getTrash();
            trash[el.article]--;
            if (trash[el.article] === 0) {
              delete trash[el.article];
            }
            setTrash(trash);
          }
          return el;
        });
      });

    default:
      return state;
  }
};

export default reducer;
