import { PureComponent } from "react";
import styles from "./Header.module.scss";
import cartIcon from "../../components/icons/cart-outline.svg";
import favIcon from "../../components/icons/services_favorite_favorite_star_9891.png";

class Header extends PureComponent {
  render() {
    let cartQt;
    let favouriteQt;
    if (localStorage.getItem("carts")) {
      cartQt = <span>{JSON.parse(localStorage.getItem("carts")).length}</span>;
    } else {
      cartQt = <span>0</span>;
    }

    if (localStorage.getItem("favourites")) {
      favouriteQt = (
        <span>{JSON.parse(localStorage.getItem("favourites")).length}</span>
      );
    } else {
      favouriteQt = <span>0</span>;
    }

    return (
      <header className={styles.root}>
        <h1>SALE OF LAPTOPS!</h1>
        <ul>
          <li>
            <a href="#">
              <img src={cartIcon} alt="Cart" />
            </a>
          </li>
          <li>{cartQt}</li>
        </ul>
        <ul>
          <li>
            <a href="#">
              <img src={favIcon} alt="Cart" width={26} />
            </a>
          </li>
          <li>{favouriteQt}</li>
        </ul>
      </header>
    );
  }
}

export default Header;

