import { PureComponent } from "react";
import styles from "./CardItem.module.scss";
import Button from "../Button/Button";
import starIcon from "../../components/icons/star_favorite_favorite_favorite_favorite_2359.png";
import outlineStarIcon from "../../components/icons/services_favorite_favorite_star_9891.png";


class CardItem extends PureComponent {
  render() {
    const {
      name,
      path,
      price,
      article,
      favourite,
      openFirstModal,
      setModalProps,
      addToFavourites,
      firstModalDisplay
    } = this.props;
    
    return (
      <div className={styles.card}>
        <button type="button" className={styles.likeButton}>
          <img
            onClick={() => {
              setModalProps({ article });
              addToFavourites(article);
            }}
            src={favourite ? outlineStarIcon : starIcon}
            alt="Favourite"
          />
        </button>
        <span className={styles.title}>{name}</span>
        <img className={styles.itemAvatar} src={path} alt={name} />
        <span className={styles.description}>{price}</span>

        <div className={styles.btnContainer}>
          <div className="app__buttons">
            <Button
              backgroundColor="rgb(219 131 49)"
              text="Add to cart"
              className="modalButton"
              onClick={() => {
                console.log(firstModalDisplay);

                setModalProps({ article, name });
                openFirstModal();
                
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CardItem;