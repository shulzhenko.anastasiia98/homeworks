import React from "react";
import styles from "./Button.module.scss"


class Button extends React.PureComponent {
    render() {
      const { backgroundColor, text, onClick, className } = this.props;
  
      return (
        <button
          className={styles.modalButton}
          style={{ backgroundColor }}
          onClick={onClick}
        >
          {text}
        </button>
      );
    }
  }
  
export default Button
