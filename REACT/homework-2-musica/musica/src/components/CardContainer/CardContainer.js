import React, { PureComponent } from "react";
import CardItem from "../CardItem/CardItem";
import styles from "./CardContainer.module.scss";

class CardContainer extends PureComponent {
  render() {
    const {
      firstModalDisplay,
      addToCart,
      cards,
      openFirstModal,
      setModalProps,
      addToFavourites,
      favourites,
    } = this.props;
    
    return (
      <div>
        <ul className={styles.list}>
          {cards.map(({ name, path, price, article, isFavourite }) => (
            <li key={article}>
              <CardItem
                favourite={favourites.includes(article)}
                addToCart={addToCart}
                article={article}
                name={name}
                price={price}
                path={path}
                isFavourite={isFavourite}
                openFirstModal={openFirstModal}
                setModalProps={setModalProps}
                addToFavourites={addToFavourites}
                firstModalDisplay={firstModalDisplay}
              />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

  
  export default CardContainer;
  