import React from "react";
import styles from "./Modal.module.scss";

class Modal extends React.PureComponent {
  render() {
    const { firstModalDisplay, closeButton, actions, close, modalProps } =
      this.props;
      if(firstModalDisplay){
    return (
      <div className={styles.modalBackground} onClick={close}>
        <div
          onClick={(e) => {
            e.stopPropagation();
          }}
          className={styles.modalContent}
        >
          <header className={styles.modalContentHeader}>
            {closeButton && (
              <div onClick={close} className={styles.modalCloseBtn}>
                &times;
              </div>
            )}
          </header>
          <div>
            <p className={styles.modalContentText}>
              Додати товар {modalProps.name} в кошик?
            </p>
            <div className={styles.modalButtons}>{actions}</div>
          </div>
        </div>
      </div>
    );
  }
  }
}

export default Modal;

