import React from "react";
import "../components/App.css";
import Button from "./Button/Button";
import Modal from "./Modal/Modal";

class App extends React.PureComponent {
  state = {
    firstModal: false,
    secondModal: false,
  };

  openFirstModal = () => {
    this.setState({ firstModal: true });
  };

  closeFirstModal = () => {
    this.setState({ firstModal: false });
  };

  openSecondModal = () => {
    this.setState({ secondModal: true });
  };

  closeSecondModal = () => {
    this.setState({ secondModal: false });
  };

  render() {
    const { firstModal, secondModal } = this.state;

    return (
      <div className="App">
        <div className="mainButtonsContainer">
        <Button
          text="Open first modal"
          backgroundColor="rgb(100 126 169)"
          handleClick={this.openFirstModal}
        />
        <Button
          text="Open second modal"
          backgroundColor="rgb(160, 206, 141)"
          handleClick={this.openSecondModal}
        />
        </div>
        {firstModal && (
          <Modal
            modal={firstModal}
            text="This text is for first modal. Do you want to continue?"
            closeButton={true}
            closeWindow={this.closeFirstModal}
            header="First modal header"
            actions={[
              <Button 
              text="OK"
              backgroundColor="rgb(179 55 43)"
              handleClick={this.closeFirstModal}/>,
              <Button 
              text="CANCEL"
              backgroundColor="rgb(179 55 43)"
              handleClick={this.closeFirstModal}/>
            ]}
          />
        )}

        {secondModal && (
          <Modal
            modal={secondModal}
            text="This text is for second modal. Do you want to continue?"
            closeButton={false}
            closeWindow={this.closeSecondModal}
            header="Second modal header"
            actions={[
              <Button 
              text="GOOD"
              backgroundColor="rgb(229 192 198)"
              handleClick={this.closeSecondModal}
              />,
              <Button 
              text="CANCEL"
              backgroundColor="rgb(229 192 198)"
              handleClick={this.closeSecondModal}
              />
            ]}
          />
        )}
      </div>
    );
  }
}

export default App;
