import React from "react";
import styles from "./Modal.module.scss"

class Modal extends React.PureComponent{
    render(){

        const { modal, text, header, closeButton, closeWindow, actions } = this.props;

        if(modal){
            return (
                <div className={styles.modal} onClick={ closeWindow } >
                    <div className={styles.modal__content} onClick={ (e) => {e.stopPropagation()}} >
                        <div className={styles.modal__header}>
                            {header}
                            {closeButton && (
                                <div onClick={ closeWindow }>X</div>
                            )}
                        </div>
                        <div className={styles.modal__text}>{ text }</div>
                        <div className={styles.modal__buttonsContainer}>{ actions }</div>
                    </div>
                </div>
            )
        }   
    }
}
export default Modal;