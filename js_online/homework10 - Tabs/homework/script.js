const showText = (event) => {
    document.querySelector(".tabs-title.active-tab").classList.remove("active-tab");
    event.target.classList.add("active-tab");

    const category = event.target.dataset.category;

    document.querySelector(`.tabs-content li.text-active`).classList.remove("text-active");
    document.querySelector(`[data-content="${category}"]`).classList.add("text-active");    
}

const tabsCollection = document.querySelectorAll(".tabs li");   
tabsCollection.forEach(tab => tab.addEventListener("click", showText));

