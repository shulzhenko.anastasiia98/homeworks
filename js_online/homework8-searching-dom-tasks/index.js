// 1.Опишіть своїми словами що таке Document Object Model (DOM).
// Це програмний інтерфейс, який дозволяє скриптам мати зв'язок з HTML документом.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// InnerText нам вертатиме лише текст, який вкладено в тег, якщо наприклад цей текст буде всередині ще одного тегу. А от innerHTML відобразить нам весь зміст тегу, включаючи додаткові теги.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// До елемента сторінки можна звернутись через document.querySelector(або document.querySelectorAll, який виокремлює всі елементи, що мають заданий селектор та утворює псевдомасив з цих елементів) або document.getElementsBy (Цим способом можна шукати елементи через id або name). Чесно кажучи, поки що не можу зробити висновок на рахунок того, який з них кращий. 


// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000.

let paragraphs = document.getElementsByTagName("p");

for (let element of paragraphs) {
    element.style.backgroundColor = '#ff0000';
}
console.log(paragraphs);


// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

const getElementById = document.querySelector('#optionsList');
console.log(getElementById);

const parentElement = getElementById.parentNode;
console.log(parentElement);

let getChildElementById = getElementById.children;

for (let element of getChildElementById) {
    console.log(element);
}


// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - 'This is a paragraph'.

const getTestParagraph = document.querySelector('.test-paragraph').innerText = "This is a paragraph";


// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.

const getElementsInMainHeader = document.querySelector('.main-header');
console.log(getElementsInMainHeader);
let getChildMainHeader = getElementsInMainHeader.children;
console.log(getChildMainHeader);

for (let elem of getChildMainHeader) {
    // if(elem.classList){
    //     elem.classList.add("nav-item")
    // }
    elem.classList.add("nav-item");
}


// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let elements = document.querySelectorAll(".products-list-item");
const elements1 = document.getElementsByClassName("products-list-item")
console.log(elements);

for (let element of elements) {
    element.classList.remove("products-list-item");
}