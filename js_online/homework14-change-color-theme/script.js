const headerBtn = document.querySelector(".header-button");
const pageBody = document.querySelector("body");
const linkCollection = document.querySelectorAll("a");

headerBtn.addEventListener("click", (event) => {
    pageBody.classList.toggle("light-theme");
    linkCollection.forEach((link) => {
        link.classList.toggle("light-theme")
    });
    event.target.classList.toggle("light-theme");

    localStorage.setItem("themeLight", JSON.stringify(pageBody.classList.contains("light-theme")));
})

window.addEventListener("load",(event) => {
    if(localStorage.getItem("themeLight") === "true"){
        pageBody.classList.toggle("light-theme");
        linkCollection.forEach((link) => {
            link.classList.toggle("light-theme")
        });
        headerBtn.classList.toggle("light-theme");

    }  
})