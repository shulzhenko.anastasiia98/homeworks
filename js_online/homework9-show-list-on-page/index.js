// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// Створення нового тегу на сторінці можливе через document.createElement('тег').Далі ми можемо встановити значення для цього елементу, наприклад через innerText. Далы ми можемо вставити цей елемент в DOM через методи вставки: node.append(вставляэ в середині елементу в кінці), node.prepend(вставляє всередині елементу напочатку), node.before(вставляє перед елементом), node.after(вставляє після елементу), node.replaceWith(замінює елемент).
// Ще один варіант це insertAdjacentHTML, але його використовувати є сенс, якщо нам потрібно лише відмалювати в DOM і ми не будемо взаємодіяти з ними.

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Перший параметр функції означає місцезнаходження HTML рядка відносно елементу, в який ми його розміщуємо. Це може бути 'beforebegin' (вставляє перед елементом), 'afterbegin'(вставляє всередині елементу на початок), 'beforeend'(вставляє всередині елементу в кінці), 'afterend'(вставляє після елементу).

// 3. Як можна видалити елемент зі сторінки?
// Видалити елемент можна за допомогою node.remove().


//TASK

const cities = ["Uzgorod", "Dnipro", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const container = document.querySelector(".container");

const showCities = (cities, container = document.body) => {
    const listHtml = cities.map((item) => `<li>${item}</li>`);
    container.insertAdjacentHTML('beforeend', listHtml.join(""));
    return listHtml;
}

console.log(showCities(cities, container));


