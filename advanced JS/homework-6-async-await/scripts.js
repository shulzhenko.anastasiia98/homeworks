// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript.
// Асинхронність - це можливіть не блокувати код на певному його етапі, наприклад при запиту fetch, оскільки опрацювання запиту може зайняти дейякий час. Async await по функціоналу є аналогом fetch.then.catch, але більш зручний в застосуванні, оскільки імітується синхронне виконання коду, рядок за рядком.


//TASK
const container = document.querySelector(".container")
const button = document.querySelector(".btn")

const getIp = async () => {
    const data = await fetch("https://api.ipify.org/?format=json").then(res => res.json())
    const {ip} = data;
    return ip;
}

const showIp = async () => {

    const ip = await getIp()

    const {city, country, timezone, regionName} = await fetch(`http://ip-api.com/json/${ip}`).then(res => res.json())
    const continent = timezone.split("/")

     container.insertAdjacentHTML("beforeend", `
     <div class="user-info">
     Сontinent: ${continent[0]}<br>
     Country: ${country}<br>
     Region: ${regionName}<br>
     City: ${city}<br>
     </div>`)
}

button.addEventListener("click", showIp)