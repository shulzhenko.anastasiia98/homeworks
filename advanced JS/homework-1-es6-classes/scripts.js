// 1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript?
// Один об'єкт може бути прототипом іншого. На прикладі, якщо у об'єкта немає потрібної властивості, то JS переходить по посиланню на прототип цього об'єкта і знаходить потрібну властивість у ньому. Якщо ж у його прототипі не буде властивості, то піде до наступного (вищого) прототипу і т.д. Прототип може бути лише об'єктом або null.


// 2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
// У конструкторі класу-нащадку викликається super() для ініціалізації батьківського конструктора, тобто щоб наслідувати всі властивості батьківського класу.


//TASK

class Employee{
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(name){
        this._name = name;
    }

    set age(age){
        this._age = age;
    }

    set salary(salary) {
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee{
    constructor(langs, name, age, salary){
        super(name, age, salary);
        this.langs = langs; 
    }

    get salary(){
        return super.salary * 3;
    }
}

const kate = new Programmer(["js", "pyton"], "kate", "28", "2000");
const john = new Programmer(["java", "pyton"], "john", "32", "3500");
const sonya = new Programmer(["js", "java", "pyton"], "sonya", "26", "4500");

console.log(kate);
console.log(john);
console.log(sonya);


