// 1. Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Оператор try-catch слід використовувати кожного разу, коли потрібно приховати помилки від користувача, або будь-коли, коли потрібно створити власні помилки на користь користувачів.Найкращий час використовувати try-catch у тих частинах коду, підозрюється, що з будь-яких причин можуть виникнуть помилки, які ви не можете контролювати.
// Наприклад коли приходить величезний массив даних про юзерів, з яким нам потрібно працювати(наприклад відмальовувати ці дані на сторінці), і в данних може бути помилка(синтаксична або може бути відсутність деякий даних). То для того, щоб це працювало корректно, можна використати конструкцію try...catch, яка зловить помилку, не відобразить некорректний елемент, і при цьому не зупинить роботу коду. Як у прикладі з домашнім завданням.


// TASK

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

const container = document.querySelector("#root");

class Err extends Error{
    constructor(property){
        super(`PROPERTY ${property} IS NOT DEFINED!`);
    }
}

class Book {
    constructor(author, name, price){
        this.author = author;
        this.name = name;
        this.price = price;
    }

    render(){
            container.insertAdjacentHTML("beforeend", `
            <li>
            <div>${this.author}</div>
            <div>${this.name}</div>
            <div>${this.price}</div>
            </li>`)
                
    }
}

const errAuthor = new Err("AUTHOR");
const errName = new Err("NAME");
const errPrice = new Err("PRICE");

books.forEach((el) => {
    try{ 
        if(el.author === undefined){
            throw errAuthor
        }
        else if(el.name === undefined){
            throw errName
        }
        else if(el.price === undefined){
            throw errPrice
        }
        else{
            new Book(el.author, el.name, el.price).render()  
        }

    }
    catch (err) {
        console.log(err);    
    }
})
