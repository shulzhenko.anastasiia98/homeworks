// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// AJAX – технологія звернення до сервера без перезавантаження сторінки. Він працює, щоб приймати запити від веб-браузера, відправляти їх на сервер
// і передавать результати назад в браузер. В першу чергу AJAX корисний для форм и кнопок, пов'язаних з елементарними діями: додати в корзину, 
// підписатись, і т.д. Живий пошук – класичний приклад використання AJAX, яким користуються сучасні пошукові системи.  З AJAX користувачам не 
// потрібно перезавантажувати сторінку, щоб побачити певні зміни. Це означає, що сайт буде залишатись швидким і буде забезпечувати плавніший 
// інтерфейс користувача.


fetch('https://ajax.test-danit.com/api/swapi/films')
.then(responce => responce.json())
.then(res => {
    res.forEach(({name, characters, episodeId, openingCrawl}, index) => {
        document.querySelector(".container").insertAdjacentHTML("beforeend", 
        `<div class="card">
        <h2>"${name}"</h2>
        <div>
        <b>Description:</b><br>
        It is a ${episodeId} film of trilogy.<br>
        ${openingCrawl}
        </div>
        <div class="characters characters-${index}">CHARACTERS: </div>
        <div class="container-loader loading-${index}">
          <div class="box-loader">
            <p>l</p>
            <p>o</p>
            <p>a</p>
            <p>d</p>
            <p>i</p>
            <p>n</p>
            <p>g</p>
          </div>
        </div>`)
            const promiseArray = characters.map(url => fetch(url).then(res => res.json()))
            Promise.allSettled(promiseArray).then(val => {
                val.forEach(({value:{name:charName, url}}) => {
                    document.querySelector(`.characters-${index}`).insertAdjacentHTML("beforeend", `<a href="${url}">${charName}</a>`);
                    document.querySelector(`.loading-${index}`).style.display = "none";
                })
            })       
    })
})
