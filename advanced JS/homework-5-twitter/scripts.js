class Card {
  constructor(name, email, title, body, postId) {
    this._name = name;
    this._email = email;
    this._title = title;
    this._body = body;
    this._postId = postId;
    this.container = document.createElement("div");
    this.deleteButton = document.createElement("button");
  }

  createCards() {
    this.container.className = "card";
    this.container.innerHTML = `
        <div class="card-top">
        <div class="name-avatar">
        <p class="avatar"></p>
        <p class="name">${this._name}</p></div>
        <div class="email">${this._email}</div>
        </div>
        <div class="card-body">
        <h2 class="title">${this._title}</h2>
        
        <div class="text">${this._body}</div>
        </div>
        `;

    this.deleteButton.innerHTML = "Delete";
    this.container.append(this.deleteButton);

    this.deleteButton.addEventListener("click", () => {
      fetch(`https://ajax.test-danit.com/api/json/posts/${this._postId}`, {
        method: "DELETE",
      })
          .then(({ status}) => {
          if (status === 200) {
            this.container.remove()
          }
      })
    });
  }

  render() {
    this.createCards();
    document.querySelector(".container").prepend(this.container);
  }
}

const createCards = () => {
  fetch("https://ajax.test-danit.com/api/json/posts")
    .then((responce) => responce.json())
    .then((posts) => {
      posts.forEach(({ title, body, userId, id: postId }) => {
        fetch("https://ajax.test-danit.com/api/json/users")
          .then((responce) => responce.json())
          .then((users) => {
            users.forEach(({ name, email, id }) => {
              if (userId === id) {
                new Card(name, email, title, body, postId).render();
              }
            });
          });
      });
    })
    .catch((err) => {
      alert("Something went wrong! Please, try to reload the page");
    });
};
createCards();
